const BijayToken = artifacts.require("BijayToken");

contract("BijayToken", (accounts) => {
  let bijayTokenInstance;
  const owner = accounts[0];
  const candidate1 = accounts[1];
  const candidate2 = accounts[2];

  beforeEach(async () => {
    bijayTokenInstance = await BijayToken.new({ from: owner });
  });

  it("should have the correct initial supply", async () => {
    const totalSupply = await bijayTokenInstance.totalSupply();
    assert.equal(totalSupply, 100, "Initial supply is not 100");
  });

  it("should register a candidate", async () => {
    const candidateName = "Alice";

    await bijayTokenInstance.registerCandidate(candidateName, { from: candidate1 });
    const isRegistered = await bijayTokenInstance.registeredCandidate(candidate1);
    assert.equal(isRegistered, true, "Candidate should be registered");

    const candidate = await bijayTokenInstance.candidates(1);
    assert.equal(candidate.id.toNumber(), 1, "Candidate ID is incorrect");
    assert.equal(candidate.name, candidateName, "Candidate name is incorrect");
    assert.equal(candidate.candidateAddress, candidate1, "Candidate address is incorrect");
  });

  it("should vote for a candidate", async () => {
    const candidateName = "Alice";

    await bijayTokenInstance.registerCandidate(candidateName, { from: candidate1 });
    await bijayTokenInstance.registerCandidate("Bob", { from: candidate2 });

    await bijayTokenInstance.vote(1, { from: owner });
    const voted = await bijayTokenInstance.voted(owner);
    assert.equal(voted, true, "Owner should have voted");

    const ownerBalance = await bijayTokenInstance.balanceOf(owner);
    const candidateBalance = await bijayTokenInstance.balanceOf(candidate1);
    assert.equal(ownerBalance.toNumber(), 99, "Owner balance is incorrect after voting");
    assert.equal(candidateBalance.toNumber(), 1, "Candidate balance is incorrect after voting");
  });
});
