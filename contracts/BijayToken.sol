// SPDX-License-Identifier: MIT
pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract BijayToken is ERC20 {
    string public name = "voting";
    string public symbol = "VS";
    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 10000000;
    uint256 public numberOfCandidates;
    address public contractOwner;

    struct Candidate {
        uint256 id;
        string name;
        address candidateAddress;
    }

    mapping(uint256 => Candidate) public candidates;
    mapping(address => bool) public registeredCandidate;
    mapping(address => bool) public voted;

    constructor() public {
        contractOwner = msg.sender;
        numberOfCandidates = 0;
        _mint(msg.sender, 100);
    }

    function vote(uint256 candidateID) public {
        require(voted[msg.sender] == false, "You have already voted");
        _transfer(contractOwner, candidates[candidateID].candidateAddress, 1);
        voted[msg.sender] = true;
    }

    // Function to register a candidate with name and ID
    function registerCandidate(string memory candidateName) public {
        require(!registeredCandidate[msg.sender], "You are already registered");

        // Create a new candidate
        uint256 candidateId = numberOfCandidates + 1;
        candidates[candidateId] = Candidate({
            id: candidateId,
            name: candidateName,
            candidateAddress: msg.sender
        });

        // Mark the address as registered
        registeredCandidate[msg.sender] = true;

        numberOfCandidates++;
    }
}
